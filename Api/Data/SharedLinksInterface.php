<?php
/**
 * @author Aleš Cankar
 * @copyright Copyright (c) 2018 Optiweb (https://www.optiweb.com)
 */

namespace Optiweb\ExtendedShelf\Api\Data;

interface SharedLinksInterface
{
    const SHARED_LINK_ID = 'shared_link_id';
    const SENDER_NAME = 'sender_name';
    const URL = 'url';
    const SENT_TO = 'sent_to';
    const SENT_AT = 'sent_at';
    const TOTAL_CART_VALUE = 'total_cart_value';
    const SHARED_LINK_CODE = 'shared_link_code';

    public function getSharedLinkId();

    public function setSharedLinkId($sharedLinkId);

    public function getSenderName();

    public function setSenderName($senderName);

    public function getUrl();

    public function setUrl($url);

    public function getSentTo();

    public function setSentTo($sentTo);

    public function getSentAt();

    public function setSentAt($sentAt);

    public function getTotalCartValue();

    public function setTotalCartValue($totalCartValue);

    public function getSharedLinkCode();

    public function setSharedLinkCode($sharedLinkCode);
}