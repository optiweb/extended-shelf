<?php
/**
 * @author Aleš Cankar
 * @copyright Copyright (c) 2018 Optiweb (https://www.optiweb.com)
 */

namespace Optiweb\ExtendedShelf\Helper;

use Magento\Store\Model\ScopeInterface;

class Data extends \Magento\Framework\App\Helper\AbstractHelper
{
    const RECAPTCHA_CONFIG_PATH = 'extended_shelf/recaptcha';
    const BITLY_TOKEN_CONFIG_PATH = 'extended_shelf/general/bitly_token';

    protected $_date;
    protected $_curl;

    /**
     * Data constructor.
     * @param \Magento\Framework\App\Helper\Context $context
     * @param \Magento\Framework\Stdlib\DateTime\DateTime $date
     * @param \Magento\Framework\HTTP\Client\Curl $curl
     */
    public function __construct(
        \Magento\Framework\App\Helper\Context $context,
        \Magento\Framework\Stdlib\DateTime\DateTime $date,
        \Magento\Framework\HTTP\Client\Curl $curl
    ) {
        $this->_date = $date;
        $this->_curl = $curl;
        parent::__construct($context);
    }

    /**
     * @return string
     */
    public function getTimeAccordingToTimeZone()
    {
        return $date = $this->_date->gmtDate();
    }

    /**
     * @param $longUrl
     * @return string
     */
    public function shortenUrl($longUrl)
    {
        $token = $this->scopeConfig->getValue(self::BITLY_TOKEN_CONFIG_PATH, ScopeInterface::SCOPE_STORE);
        if (!$token) {
            return $longUrl;
        }
        $this->_curl->get('https://api-ssl.bitly.com/v3/shorten?longUrl=' . urlencode($longUrl). '&access_token=' . $token);
        $response = json_decode($this->_curl->getBody(), true);
        if ($response['status_code'] == 200) {
            return $response['data']['url'];
        }
        return $longUrl;
    }

}
