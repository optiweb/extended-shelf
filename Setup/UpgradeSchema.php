<?php
/**
 * @author Aleš Cankar
 * @copyright Copyright (c) 2018 Optiweb (https://www.optiweb.com)
 */

namespace Optiweb\ExtendedShelf\Setup;

use Magento\Framework\Setup\UpgradeSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface as ModuleContext;
use Magento\Framework\Setup\SchemaSetupInterface as SchemaSetup;

class UpgradeSchema implements UpgradeSchemaInterface
{
    /**
     * @param SchemaSetup $setup
     * @param ModuleContext $context
     */
    public function upgrade(SchemaSetup $setup, ModuleContext $context)
    {
        $installer = $setup;
        $installer->startSetup();

        if (version_compare($context->getVersion(), '1.0.1', '<')) {
            $installer->getConnection()->modifyColumn(
                $installer->getTable('optiweb_shared_links'),
                'total_cart_value',
                [
                    'type' => \Magento\Framework\DB\Ddl\Table::TYPE_DECIMAL,
                    'length' => '12,4',
                    'nullable' => true,
                    'comment' => 'Cart Value'
                ]
            );
        }
        $installer->endSetup();
    }
}