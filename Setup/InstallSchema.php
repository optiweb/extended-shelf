<?php
/**
 * @author Aleš Cankar
 * @copyright Copyright (c) 2018 Optiweb (https://www.optiweb.com)
 */

namespace Optiweb\ExtendedShelf\Setup;

class InstallSchema implements \Magento\Framework\Setup\InstallSchemaInterface
{
    /**
     * @param \Magento\Framework\Setup\SchemaSetupInterface $setup
     * @param \Magento\Framework\Setup\ModuleContextInterface $context
     * @throws \Zend_Db_Exception
     */
    public function install(
        \Magento\Framework\Setup\SchemaSetupInterface $setup,
        \Magento\Framework\Setup\ModuleContextInterface $context)
    {
        $installer = $setup;
        $installer->startSetup();
        if (!$installer->tableExists('optiweb_shared_links')) {
            $table = $installer->getConnection()
                ->newTable($installer->getTable('optiweb_shared_links'))
                ->addColumn(
                    'shared_link_id',
                    \Magento\Framework\DB\Ddl\Table::TYPE_BIGINT,
                    null,
                    [
                        'identity' => true,
                        'nullable' => false,
                        'primary'  => true,
                        'unsigned' => true,
                    ],
                    'Shared Link Id'
                )
                ->addColumn(
                    'sender_name',
                    \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                    255,
                    ['nullable => false'],
                    'Sender Name'
                )
                ->addColumn(
                    'url',
                    \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                    255,
                    [],
                    'Shared Link'
                )
                ->addColumn(
                    'sent_to',
                    \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                    255,
                    [],
                    'Sent To'
                )
                ->addColumn(
                    'total_cart_value',
                    \Magento\Framework\DB\Ddl\Table::TYPE_DECIMAL,
                    4,
                    [],
                    'Cart Value'
                )
                ->addColumn(
                    'shared_link_code',
                    \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                    255,
                    [],
                    'Shared Link Code'
                )
                ->addColumn(
                    'sent_at',
                    \Magento\Framework\DB\Ddl\Table::TYPE_TIMESTAMP,
                    null,
                    [],
                    'Link Shared At'
                )
                ->setComment('Shared Links Table');
            $installer->getConnection()->createTable($table);
        }
        $installer->endSetup();
    }
}