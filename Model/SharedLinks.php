<?php
/**
 * @author Aleš Cankar
 * @copyright Copyright (c) 2018 Optiweb (https://www.optiweb.com)
 */

namespace Optiweb\ExtendedShelf\Model;

use Optiweb\ExtendedShelf\Api\Data\SharedLinksInterface;

class SharedLinks extends \Magento\Framework\Model\AbstractModel implements SharedLinksInterface
{
    /**
     * CMS page cache tag.
     */
    const CACHE_TAG = 'shared_links';

    /**
     * @var string
     */
    protected $_cacheTag = 'shared_links';

    /**
     * Prefix of model events names.
     * @var string
     */
    protected $_eventPrefix = 'shared_links';

    /**
     * Initialize resource model.
     */
    protected function _construct()
    {
        $this->_init('Optiweb\ExtendedShelf\Model\ResourceModel\SharedLinks');
    }

    public function getSharedLinkId()
    {
        return $this->getData(self::SHARED_LINK_ID);
    }

    public function setSharedLinkId($sharedLinkId)
    {
        return $this->setData(self::SHARED_LINK_ID,$sharedLinkId);
    }
    public function getSenderName()
    {
        return $this->getData(self::SENDER_NAME);
    }

    public function setSenderName($senderName)
    {
        return $this->setData(self::SENDER_NAME,$senderName);
    }

    public function getUrl()
    {
        return $this->getData(self::URL);
    }

    public function setUrl($url)
    {
        return $this->setData(self::URL,$url);
    }

    public function getSentTo()
    {
        return $this->getData(self::SENT_TO);
    }

    public function setSentTo($sentTo)
    {
        return $this->setData(self::SENT_TO,$sentTo);
    }

    public function getSentAt()
    {
        return $this->getData(self::SENT_AT);
    }

    public function setSentAt($sentAt)
    {
        return $this->setData(self::SENT_AT,$sentAt);
    }

    public function getTotalCartValue()
    {
        return $this->getData(self::TOTAL_CART_VALUE);
    }

    public function setTotalCartValue($totalCartValue)
    {
        return $this->setData(self::TOTAL_CART_VALUE, $totalCartValue);
    }

    public function getSharedLinkCode()
    {
        return $this->getData(self::SHARED_LINK_CODE);
    }

    public function setSharedLinkCode($sharedLinkCode)
    {
        return $this->setData(self::SHARED_LINK_CODE, $sharedLinkCode);
    }
}