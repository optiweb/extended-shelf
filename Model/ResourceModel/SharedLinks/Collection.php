<?php
/**
 * @author Aleš Cankar
 * @copyright Copyright (c) 2018 Optiweb (https://www.optiweb.com)
 */

namespace Optiweb\ExtendedShelf\Model\ResourceModel\SharedLinks;

class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{
    protected $_idFieldName = \Optiweb\ExtendedShelf\Model\SharedLinks::SHARED_LINK_ID;
    protected $_eventPrefix = 'extendedshelf_grid_collection';
    protected $_eventObject = 'extendedshelf_collection';

    /**
     * Define resource model.
     */
    protected function _construct()
    {
        $this->_init('Optiweb\ExtendedShelf\Model\SharedLinks', 'Optiweb\ExtendedShelf\Model\ResourceModel\SharedLinks');
    }
}