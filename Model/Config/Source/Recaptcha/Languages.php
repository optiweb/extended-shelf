<?php
/**
 * @author Aleš Cankar
 * @copyright Copyright (c) 2018 Optiweb (https://www.optiweb.com)
 * @url https://developers.google.com/recaptcha/docs/language
 */

namespace Optiweb\ExtendedShelf\Model\Config\Source\Recaptcha;

class Languages implements \Magento\Framework\Option\ArrayInterface
{
    /**
     * Official supported languages by Google ReCaptcha
     * @return array
     */
    public function toOptionArray()
    {
        return [
            ['value' => 'ar', 'label' => 'Arabic'],
            ['value' => 'af', 'label' => 'Afrikaans'],
            ['value' => 'am', 'label' => 'Amharic'],
            ['value' => 'hy', 'label' => 'Armenian'],
            ['value' => 'az', 'label' => 'Azerbaijani'],
            ['value' => 'eu', 'label' => 'Basque'],
            ['value' => 'bn', 'label' => 'Bengali'],
            ['value' => 'bg', 'label' => 'Bulgarian'],
            ['value' => 'ca', 'label' => 'Catalan'],
            ['value' => 'zh-HK', 'label' => 'Chinese (Hong Kong)'],
            ['value' => 'zh-CN', 'label' => 'Chinese (Simplified)'],
            ['value' => 'zh-TW', 'label' => 'Chinese (Traditional)'],
            ['value' => 'hr', 'label' => 'Croatian'],
            ['value' => 'cs', 'label' => 'Czech'],
            ['value' => 'da', 'label' => 'Danish'],
            ['value' => 'nl', 'label' => 'Dutch'],
            ['value' => 'en-GB', 'label' => 'English (UK)'],
            ['value' => 'en', 'label' => 'English (US)'],
            ['value' => 'et', 'label' => 'Estonian'],
            ['value' => 'fil', 'label' => 'Filipino'],
            ['value' => 'fi', 'label' => 'Finnish'],
            ['value' => 'fr', 'label' => 'French'],
            ['value' => 'fr-CA', 'label' => 'French (Canadian)'],
            ['value' => 'gl', 'label' => 'Galician'],
            ['value' => 'ka', 'label' => 'Georgian'],
            ['value' => 'de', 'label' => 'German'],
            ['value' => 'de-AT', 'label' => 'German (Austria)'],
            ['value' => 'de-CH', 'label' => 'German (Switzerland)'],
            ['value' => 'el', 'label' => 'Greek'],
            ['value' => 'gu', 'label' => 'Gujarati'],
            ['value' => 'iw', 'label' => 'Hebrew'],
            ['value' => 'hi', 'label' => 'Hindi'],
            ['value' => 'hu', 'label' => 'Hungarain'],
            ['value' => 'is', 'label' => 'Icelandic'],
            ['value' => 'id', 'label' => 'Indonesian'],
            ['value' => 'it', 'label' => 'Italian'],
            ['value' => 'ja', 'label' => 'Japanese'],
            ['value' => 'kn', 'label' => 'Kannada'],
            ['value' => 'ko', 'label' => 'Korean'],
            ['value' => 'lo', 'label' => 'Laothian'],
            ['value' => 'lv', 'label' => 'Latvian'],
            ['value' => 'lt', 'label' => 'Lithuanian'],
            ['value' => 'ms', 'label' => 'Malay'],
            ['value' => 'ml', 'label' => 'Malayalam'],
            ['value' => 'mr', 'label' => 'Marathi'],
            ['value' => 'mn', 'label' => 'Mongolian'],
            ['value' => 'no', 'label' => 'Norwegian'],
            ['value' => 'fa', 'label' => 'Persian'],
            ['value' => 'pl', 'label' => 'Polish'],
            ['value' => 'pt', 'label' => 'Portuguese'],
            ['value' => 'pt-BR', 'label' => 'Portuguese (Brazil)'],
            ['value' => 'pt-PT', 'label' => 'Portuguese (Portugal)'],
            ['value' => 'ro', 'label' => 'Romanian'],
            ['value' => 'ru', 'label' => 'Russian'],
            ['value' => 'sr', 'label' => 'Serbian'],
            ['value' => 'si', 'label' => 'Sinhalese'],
            ['value' => 'sk', 'label' => 'Slovak'],
            ['value' => 'sl', 'label' => 'Slovenian'],
            ['value' => 'es', 'label' => 'Spanish'],
            ['value' => 'es-419', 'label' => 'Spanish (Latin America)'],
            ['value' => 'sw', 'label' => 'Swahili'],
            ['value' => 'sv', 'label' => 'Swedish'],
            ['value' => 'ta', 'label' => 'Tamil'],
            ['value' => 'te', 'label' => 'Telugu'],
            ['value' => 'th', 'label' => 'Thai'],
            ['value' => 'tr', 'label' => 'Turkish'],
            ['value' => 'uk', 'label' => 'Ukrainian'],
            ['value' => 'ur', 'label' => 'Urdu'],
            ['value' => 'vi', 'label' => 'Vietnamese'],
            ['value' => 'zu', 'label' => 'Zulu'],
        ];
    }
}