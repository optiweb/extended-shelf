<?php
/**
 * @author Aleš Cankar
 * @copyright Copyright (c) 2018 Optiweb (https://www.optiweb.com)
 */

namespace Optiweb\ExtendedShelf\Block;

use Magento\Framework\View\Element\Template;
use Magento\Store\Model\ScopeInterface;
use Optiweb\ExtendedShelf\Helper\Data as Helper;

class Form extends Template
{
    const SHARE_CART_SEND_URL = 'extendedshelf/form/send';

    public function __construct(Template\Context $context)
    {
        parent::__construct($context);
    }

    /**
     * @return array
     */
    public function getRecaptchaConfig()
    {
        return $this->_scopeConfig->getValue(Helper::RECAPTCHA_CONFIG_PATH, ScopeInterface::SCOPE_STORE);
    }
}