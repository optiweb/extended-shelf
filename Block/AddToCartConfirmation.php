<?php
/**
 * @author Aleš Cankar
 * @copyright Copyright (c) 2018 Optiweb (https://www.optiweb.com)
 */

namespace Optiweb\ExtendedShelf\Block;

use Magento\Catalog\Model\ProductRepository;
use Magento\Catalog\Model\Product;
use Magento\Framework\View\Element\Template;

class AddToCartConfirmation extends Template
{
    protected $_product;
    protected $_configurable;
    protected $_bundleType;
    protected $_downloadable;
    protected $_items;
    protected $_imageHelper;
    protected $_listProduct;

    /**
     * AddToCartConfirmation constructor.
     * @param Template\Context $context
     * @param ProductRepository $productRepository
     * @param \Magento\ConfigurableProduct\Model\Product\Type\Configurable $configurable
     * @param \Magento\Bundle\Model\Product\Type $bundleProductType
     * @param \Magento\Downloadable\Model\Product\Type $downloadable
     * @param \Magento\Catalog\Helper\Image $imageHelper
     * @param \Magento\Catalog\Block\Product\ListProduct $listProduct
     */
    public function __construct(
        Template\Context $context,
        ProductRepository $productRepository,
        \Magento\ConfigurableProduct\Model\Product\Type\Configurable $configurable,
        \Magento\Bundle\Model\Product\Type $bundleProductType,
        \Magento\Downloadable\Model\Product\Type $downloadable,
        \Magento\Catalog\Helper\Image $imageHelper,
        \Magento\Catalog\Block\Product\ListProduct $listProduct
    ) {
        $this->_product = $productRepository;
        $this->_configurable = $configurable;
        $this->_bundleType = $bundleProductType;
        $this->_downloadable = $downloadable;
        $this->_imageHelper = $imageHelper;
        $this->_listProduct = $listProduct;
        parent::__construct($context);
    }

    /**
     * @return array
     */
    public function getCartItems()
    {
        $this->_items = $this->getRequest()->getParams();
        return $this->_items;
    }

    /**
     * @param int $id
     * @return bool|\Magento\Catalog\Api\Data\ProductInterface|mixed
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function getProduct(int $id)
    {
        if (!$id) {
            return false;
        }
        return $this->_product->getById($id);
    }

    /**
     * @param Product $product
     * @return array
     */
    public function getConfigurableOptions(Product $product)
    {
        return $this->_configurable->getConfigurableAttributesAsArray($product);
    }

    /**
     * @param Product $product
     * @return \Magento\Bundle\Model\ResourceModel\Selection\Collection
     */
    public function getBundleOptions(Product $product)
    {
        $selections = $this->_bundleType->getSelectionsCollection($this->_bundleType->getOptionsIds($product), $product);
        return $selections;
    }

    /**
     * @param Product $product
     * @return string
     */
    public function getImageUrl(Product $product)
    {
        return $this->_imageHelper->init($product, 'product_thumbnail_image', ['type' => 'thumbnail'])
            ->keepAspectRatio(true)
            ->getUrl();
    }

    /**
     * @param Product $product
     * @return \Magento\Downloadable\Model\Link[]
     */
    public function getDownloadableLinks(Product $product)
    {
        return $this->_downloadable->getLinks($product);
    }

    /**
     * @param Product $product
     * @return string
     */
    public function getPrice(Product $product)
    {
        return $this->_listProduct->getProductPrice($product);
    }

}