<?php
/**
 * @author Aleš Cankar
 * @copyright Copyright (c) 2018 Optiweb (https://www.optiweb.com)
 */

namespace Optiweb\ExtendedShelf\Controller\Form;

use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Action\Context;
use Magento\Framework\Mail\Template\TransportBuilder;
use Magento\Framework\Translate\Inline\StateInterface;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Optiweb\ExtendedShelf\Helper\Data as Helper;
use Optiweb\ExtendedShelf\Model\SharedLinksFactory;
use Magento\Framework\Controller\Result\JsonFactory;
use Magento\Store\Model\ScopeInterface;
use Magento\Framework\HTTP\Client\Curl;

use Magento\Store\Model\StoreManagerInterface as Storemanager;
use Magento\Framework\Webapi\Exception as HTTPException;

class Send extends Action
{
    protected $_transportBuilder;
    protected $_inlineTranslation;
    protected $_sharedLinksFactory;
    protected $_resultJsonFactory;
    protected $_curl;
    protected $_store;
    protected $_scopeConfig;
    protected $_pricing;
    protected $_session;
    protected $_esHelper;

    const SHARE_CART_CONFIRM_URL = 'extendedshelf/index/addtocartconfirmation';

    /**
     * Send constructor.
     * @param Context $context
     * @param TransportBuilder $transportBuilder
     * @param StateInterface $inlineTranslation
     * @param ScopeConfigInterface $scopeConfig
     * @param SharedLinksFactory $sharedLinksFactory
     * @param JsonFactory $jsonFactory
     * @param Storemanager $storeManager
     * @param Curl $curl
     * @param \Magento\Framework\Pricing\Helper\Data $pricing
     * @param \Magento\Checkout\Model\Session $session
     * @param Helper $helper
     */
    public function __construct(
        Context $context,
        TransportBuilder $transportBuilder,
        StateInterface $inlineTranslation,
        ScopeConfigInterface $scopeConfig,
        SharedLinksFactory $sharedLinksFactory,
        JsonFactory $jsonFactory,
        Storemanager $storeManager,
        Curl $curl,
        \Magento\Framework\Pricing\Helper\Data $pricing,
        \Magento\Checkout\Model\Session $session,
        Helper $helper
    ) {
        $this->_transportBuilder = $transportBuilder;
        $this->_inlineTranslation = $inlineTranslation;
        $this->_scopeConfig = $scopeConfig;
        $this->_sharedLinksFactory = $sharedLinksFactory;
        $this->_resultJsonFactory = $jsonFactory;
        $this->_store = $storeManager;
        $this->_curl = $curl;
        $this->_pricing = $pricing;
        $this->_session = $session;
        $this->_esHelper = $helper;
        parent::__construct($context);
    }

    /**
     * @return \Magento\Framework\App\ResponseInterface|\Magento\Framework\Controller\Result\Json|\Magento\Framework\Controller\ResultInterface
     */
    public function execute()
    {
        $result = $this->_resultJsonFactory->create();
        $post = $this->getRequest()->getPostValue();

        if (!$post) {
            return $result->setData([
                'success' => false,
                'message' => __('Invalid request.')
            ])->setHttpResponseCode(HTTPException::HTTP_BAD_REQUEST);
        }

        $recaptchaResponse = $this->getRequest()->getParam('g-recaptcha-response');
        $recaptchaConfig = $this->_scopeConfig->getValue(Helper::RECAPTCHA_CONFIG_PATH, ScopeInterface::SCOPE_STORE);

        try {
            if ($recaptchaConfig['enable']) {
                $valid = $this->verifyRecaptcha($recaptchaConfig['secret'], $recaptchaResponse);
                if (!$valid) {
                    throw new \Exception(__('ReCaptcha verification failed.'));
                }
            }

            $name = $post['name'];
            $recipients = array_filter(array_map('trim', explode(',', $post['email'])));
            $dateTime = $this->_esHelper->getTimeAccordingToTimeZone();
            $postObject = new \Magento\Framework\DataObject();
            $postObject->setData($post);

            if (!\Zend_Validate::is(trim($name), 'NotEmpty')) {
                throw new \Exception(__('Please enter your name.'));
            }
            foreach ($recipients as $address) {
                if (!\Zend_Validate::is(trim($address), 'EmailAddress')) {
                    throw new \Exception(__('Please enter a valid email address or more addresses, comma separated.'));
                }
            }
            if (\Zend_Validate::is(trim($post['hideit']), 'NotEmpty')) {
                throw new \Exception(__('What are you doing? This field must be empty!!!'));
            }

        } catch (\Exception $e) {
            $this->messageManager->addErrorMessage($e->getMessage());
            return $result->setData([
                'success' => false,
                'message' => $e->getMessage()
            ])->setHttpResponseCode(HTTPException::HTTP_BAD_REQUEST);
        }

        $from = $this->_scopeConfig->getValue('contact/share_cart/share_from', ScopeInterface::SCOPE_STORE) ?: 'general';

        try {
            $url = $this->getShareUrl();

            $items = $this->_getCartItems();
            if (!$items) {
                return $result->setData([
                    'success' => false,
                    'message' => __('You have no items in your shopping cart.')
                ])->setHttpResponseCode(HTTPException::HTTP_BAD_REQUEST);
            }
            $this->_inlineTranslation->suspend();
            $totalCartValue = $this->_getCartValue();
            $emailWasSent = false;
            foreach ($recipients as $recipient) {
                $transport = $this->_transportBuilder
                    ->setTemplateIdentifier($this->_scopeConfig->getValue(
                        'extended_shelf/email_settings/share_cart_template',
                        ScopeInterface::SCOPE_STORE
                    ))
                    ->setTemplateOptions([
                        'area' => 'frontend',
                        'store' => $this->_store->getStore()->getId(),
                    ])
                    ->setTemplateVars([
                        'name' => $post['name'],
                        'grand_total' => $totalCartValue,
                        'share_url' => $url,
                        'items' => $items,
                    ])
                    ->setFrom($from)
                    ->addTo($recipient)
                    ->setReplyTo($from)
                    ->getTransport();
                $transport->sendMessage();
                $emailWasSent = true;
            }
        } catch (\Exception $e) {
            $this->messageManager->addErrorMessage($e->getMessage());
            $this->_inlineTranslation->resume();
            return $result->setData([
                'success' => false,
                'message' => $e->getMessage()
            ])->setHttpResponseCode(HTTPException::HTTP_INTERNAL_ERROR);
        }
        $this->_inlineTranslation->resume();

        if ($emailWasSent) {
            try {
                $this->_sharedLinksFactory->create()
                    ->setSenderName($name)
                    ->setUrl($url)
                    ->setSentTo(implode(', ', $recipients))
                    ->setSentAt($dateTime)
                    ->setTotalCartValue($totalCartValue)
                    ->save();
            } catch (\Exception $e) {
                $writer = new \Zend\Log\Writer\Stream(BP . '/var/log/test.log');
                $logger = new \Zend\Log\Logger();
                $logger->addWriter($writer);
                $logger->info('Error saving shared link:');
                $logger->info($e->getMessage());
            }
        }
        $this->messageManager->addSuccessMessage(__('Your cart has been successfully shared with %1', implode(', ', $recipients)));
        return $result->setData([
            'success' => true,
            'message' => __('Your cart has been successfully shared with %1', implode(', ', $recipients))
        ]);
    }

    /**
     * @param string $secret
     * @param string $response
     * @return bool
     */
    private function verifyRecaptcha(string $secret = '', string $response = '')
    {
        if (!$secret || !$response) {
            return false;
        }
        $this->_curl->get('https://www.google.com/recaptcha/api/siteverify?secret=' . $secret . '&response=' . $response);
        $result = json_decode($this->_curl->getBody(), true);
        return (bool)$result['success'];
    }

    /**
     * @return string
     */
    public function getShareUrl()
    {
        $quoteItemParams = $this->getQuoteParams();
        $url = $this->_url->getUrl(self::SHARE_CART_CONFIRM_URL);
        $newUrl = $url . "?" . http_build_query($quoteItemParams);
        if ($this->_scopeConfig->getValue('extended_shelf/general/enable_bitly', ScopeInterface::SCOPE_STORE)) {
            return $this->_esHelper->shortenUrl($newUrl);
        }
        return $newUrl;
    }

    /**
     * @return array
     */
    protected function getQuoteParams()
    {
        $quoteItems = $this->_getCartItems();
        $quoteItemsArray = [];

        if (!empty($quoteItems)) {
            foreach ($quoteItems as $quoteItem) {
                $product = $quoteItem->getProduct();
                $_customOptions = $quoteItem->getProduct()
                    ->getTypeInstance(true)
                    ->getOrderOptions($quoteItem->getProduct());
                $item = [
                    'type' => $product->getTypeId(),
                    'product_id' => $product->getId(),
                    'qty' => $_customOptions['info_buyRequest']['qty']
                ];
                switch ($product->getTypeId()) {
                    case 'simple' :
                        break;
                    case 'configurable' :
                        $item['super_attribute'] = $_customOptions['info_buyRequest']['super_attribute'];
                        break;
                    case 'bundle' :
                        $bundleOptions = [];
                        foreach ($_customOptions['info_buyRequest']['bundle_option'] as $i => $bundleOption) {
                            $bundleOptions[$bundleOption] = $_customOptions['info_buyRequest']['bundle_option_qty'][$i];
                        };
                        $item['bundle_options'] = $bundleOptions;
                        break;
                    case 'downloadable' :
                        $item['links'] = $_customOptions['links'];
                }
                $quoteItemsArray[] = $item;
            }
        }
        return $quoteItemsArray;
    }

    /**
     * @return \Magento\Quote\Model\Quote\Item[]
     */
    private function _getCartItems()
    {
        return $this->_session->getQuote()->getAllVisibleItems();
    }

    /**
     * @return float|string
     */
    private function _getCartValue()
    {
        $cartGrandTotal = $this->_session->getQuote()->getSubtotal();
        return $this->_pricing->currency($cartGrandTotal, true, false);
    }

}
