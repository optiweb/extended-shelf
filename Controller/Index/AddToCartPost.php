<?php
/**
 * @author Aleš Cankar
 * @copyright Copyright (c) 2018 Optiweb (https://www.optiweb.com)
 */

namespace Optiweb\ExtendedShelf\Controller\Index;

use Magento\Framework\App\Action\Context;

class AddToCartPost extends \Magento\Framework\App\Action\Action
{
    protected $_cart;
    protected $_productRepository;
    protected $_messageManager;

    /**
     * AddToCartPost constructor.
     * @param Context $context
     * @param \Magento\Checkout\Model\Cart $cart
     * @param \Magento\Catalog\Model\ProductRepository $productRepository
     * @param \Magento\Framework\Message\ManagerInterface $messageManager
     */
    public function __construct(
        Context $context,
        \Magento\Checkout\Model\Cart $cart,
        \Magento\Catalog\Model\ProductRepository $productRepository,
        \Magento\Framework\Message\ManagerInterface $messageManager
    ) {
        $this->_cart = $cart;
        $this->_productRepository = $productRepository;
        $this->_messageManager = $messageManager;
        parent::__construct($context);
    }

    /**
     * @return \Magento\Framework\App\ResponseInterface|\Magento\Framework\Controller\ResultInterface|void
     */
    public function execute()
    {
        $items = $this->getRequest()->getParams();
        foreach ($items['items'] as $id => $data) {
            if (!$data['qty']) {
                continue;
            }
            try {
                $product = $this->_productRepository->getById($id, false, null, true);
                $this->_cart->addProduct($product, $data);
            } catch (\Exception $e) {
                $this->_messageManager->addErrorMessage(__('Something went wrong!' . '<br />' . $e->getMessage()));
            }
        }
        try {
            $this->_cart->save();
            $this->_messageManager->addSuccessMessage(__('Products were successfully added to your cart.'));
        } catch (\Exception $e) {
            $this->_messageManager->addErrorMessage(__('Something went wrong while adding product to your cart.') . '<br />' . $e->getMessage());
        }
        $this->_redirect('checkout/cart');
    }
}