<?php
/**
 * @author Aleš Cankar
 * @copyright Copyright (c) 2018 Optiweb (https://www.optiweb.com)
 */

namespace Optiweb\ExtendedShelf\Controller\Adminhtml\SharedLinks;

class Index extends \Magento\Backend\App\Action
{
    /**
     * @var \Magento\Framework\View\Result\PageFactory
     */
    protected $_resultPageFactory;

    /**
     * @param \Magento\Backend\App\Action\Context        $context
     * @param \Magento\Framework\View\Result\PageFactory $resultPageFactory
     */
    public function __construct(
        \Magento\Backend\App\Action\Context $context,
        \Magento\Framework\View\Result\PageFactory $resultPageFactory
    ) {
        $this->_resultPageFactory = $resultPageFactory;
        parent::__construct($context);
    }

    /**
     * Grid List page.
     * @return \Magento\Backend\Model\View\Result\Page
     */
    public function execute()
    {
        /** @var \Magento\Backend\Model\View\Result\Page $resultPage */
        $resultPage = $this->_resultPageFactory->create();
        $resultPage->setActiveMenu('Optiweb_ExtendedShelf::shared_links');
        $resultPage->getConfig()->getTitle()->prepend(__('Shared Links'));

        return $resultPage;
    }

    /**
     * Check Grid List Permission.
     * @return bool
     */
    protected function _isAllowed()
    {
        return $this->_authorization->isAllowed('Optiweb_ExtendedShelf::shared_links');
    }
}