<?php
/**
 * @author Aleš Cankar
 * @copyright Copyright (c) 2018 Optiweb (https://www.optiweb.com)
 */

namespace Optiweb\ExtendedShelf\Controller\Adminhtml\SharedLinks;

use Magento\Backend\App\Action;

class Delete extends Action
{
    protected $_model;

    /**
     * @param Action\Context $context
     * @param \Optiweb\ExtendedShelf\Model\SharedLinks $model
     */
    public function __construct(
        Action\Context $context,
        \Optiweb\ExtendedShelf\Model\SharedLinks $model
    ) {
        $this->_model = $model;
        parent::__construct($context);
    }

    /**
     * {@inheritdoc}
     */
    protected function _isAllowed()
    {
        return $this->_authorization->isAllowed('Optiweb_ExtendedShelf::shared_links_delete');
    }

    /**
     * Delete action
     * @return \Magento\Framework\Controller\ResultInterface
     */
    public function execute()
    {
        $id = $this->getRequest()->getParam('id');
        /** @var \Magento\Backend\Model\View\Result\Redirect $resultRedirect */
        $resultRedirect = $this->resultRedirectFactory->create();
        if ($id) {
            try {
                $this->_model->load($id)->delete();
                $this->messageManager->addSuccessMessage(__('Row deleted'));
                return $resultRedirect->setPath('*/*/');
            } catch (\Exception $e) {
                $this->messageManager->addErrorMessage($e->getMessage());
                return $resultRedirect->setPath('*/*/edit', ['shared_link_id' => $id]);
            }
        }
        $this->messageManager->addErrorMessage(__('Row does not exist'));
        return $resultRedirect->setPath('*/*/');
    }
}