#Magento 2 - Extended shelf module
This extensions enables users to share their cart with their friends. A new form is added to cart sidebar where you type in your name and your friend's email address(es). An email is then sent to the recipient(s) containing a table of products and a link where they can configure the products and finally add them to their cart.

---

##Features
- Share cart with other users just by sending a link
- Fully compatible with different product types (simple, configurable, virtual, bundle)
- Ability to send to multiple people simultaneously
- Ability to reconfigure products and quantities on the confirmation page
- Link shortening and tracking using Bit.ly
- Spam protection by Google reCapthca (v2)
    - Select any language reCapthca provides
- View shared links in the dashboard (`Sales > Shared Links`)

##Compatibility
Developed on 2.2.6. Luma theme was used with very few additional extensions. Different themes shouldn't affect compatibility but your mileage may vary.

##Known issues
- currently there are no known issues.

##Installation:
0. install module: `composer require optiweb/extended_shelf`
10. enable module: `php bin/magento module:enable Optiweb_ExtendedShelf`
20. run `php bin/magento setup:upgrade` from the root directory of your Magento installation
30. configure module inside `Optiweb > Extended shelf settings`

##Usage
###Sender
0. expand the form by clicking the "Share this cart with a friend" link on the checkout cart summary column
10. insert your name and your friend's email address
20. confirm you're not a robot (if enabled)
30. hit send products

###Receiver
0. click on the link in the email
10. reconfigure product quantities and options as desired
20. click "Add these products to my cart" button

##Changelog
- `1.0.1` initial release
- `1.0.2` fix potential error on the cart if the user failed to configure the module first before trying it out. 
An error would appear `undefined index 'site_key'`, because reCaptcha is enabled by default but no `site_key` value is provided.

##Licence:
MIT. (see `LICENCE.txt`) 